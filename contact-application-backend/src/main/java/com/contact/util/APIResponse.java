package com.contact.util;

public class APIResponse {

	private Boolean sucess;
	private String message;

	public APIResponse() {
	}

	public APIResponse(Boolean sucess, String message) {
		super();
		this.sucess = sucess;
		this.message = message;
	}

	public Boolean getSucess() {
		return sucess;
	}

	public void setSucess(Boolean sucess) {
		this.sucess = sucess;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
