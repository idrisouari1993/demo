package com.contact.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.NaturalId;

@Entity
@Table(name = "person")
public class Person {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@Column(name = "firstName")
	private String firstName;

	@NotBlank
	@Column(name = "lastName")
	private String lastName;
	
	@NotBlank
	@Column(name = "adresse", length = 255)
	private String adresse;

	@NaturalId
	@NotBlank
	@Column(name = "email")
	private String email;

	@Column(name = "phone", length = 12)
	private String phone;

	public Person() {

	}

	public Person(String firstName, String lastName, String adresse, String email, String phone) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.adresse = adresse;
		this.email = email;
		this.phone = phone;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", adresse=" + adresse
				+ ", email=" + email + ", phone=" + phone + "]";
	}

}
