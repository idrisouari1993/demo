package com.contact;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContactApplicationBackend {

	public static void main(String[] args) {
		SpringApplication.run(ContactApplicationBackend.class, args);
	}
}
