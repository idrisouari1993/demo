package com.contact.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.contact.entities.Person;
import com.contact.exception.ResourceNotFoundException;
import com.contact.repository.PersonRepository;
import com.contact.services.PersonService;
import com.contact.util.APIResponse;

@RestController
@RequestMapping("/api/person")
@CrossOrigin("*")
public class PersonController {

	@Autowired
	private PersonRepository personRepository;

	@Autowired
	private PersonService personService;

	@PostMapping
	public ResponseEntity<?> savePerson(@RequestBody Person person) {
		if (personRepository.existsByEmail(person.getEmail())) {
			return new ResponseEntity<>(new APIResponse(false, "Contact is already exists."), HttpStatus.BAD_REQUEST);
		}

		Person createdPerson = personService.save(person);
		if (createdPerson == null) {
			return new ResponseEntity<>(new APIResponse(false, "Server Error !"), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(new APIResponse(true, "Contact was created successfully."), HttpStatus.CREATED);
	}

	@PutMapping
	public ResponseEntity<?> updatePerson(@RequestBody Person person) {

		if (person.getId() == null) {
			return savePerson(person);
		}

		Person updatedPerson = personService.save(person);
		if (updatedPerson == null) {
			return new ResponseEntity<>(new APIResponse(false, "Server Error !"), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(new APIResponse(true, "Contact was updated successfully."), HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<List<Person>> getAll() {

		List<Person> listOfPerson = personService.getAll();

		return new ResponseEntity<List<Person>>(listOfPerson, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Person> getById(@PathVariable("id") Long id) {

		Person person = personService.getById(id).orElseThrow(() -> new ResourceNotFoundException("Person", "id", id));

		return new ResponseEntity<Person>(person, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteById(@PathVariable("id") Long id) {
		this.personService.delete(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

}
