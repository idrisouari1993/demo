package com.contact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.contact.entities.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {
	
	public Boolean existsByEmail(String email);

}
