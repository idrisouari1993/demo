package com.contact.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.contact.entities.Person;
import com.contact.repository.PersonRepository;

@Service
public class PersonService implements IService<Person> {

	@Autowired
	private PersonRepository personRepository;

	@Override
	public Person save(Person person) {
		return this.personRepository.save(person);
	}

	@Override
	public void delete(Long id) {
		this.personRepository.deleteById(id);
	}

	@Override
	public Optional<Person> getById(Long id) {

		return this.personRepository.findById(id);
	}

	@Override
	public List<Person> getAll() {

		return this.personRepository.findAll();
	}

}
