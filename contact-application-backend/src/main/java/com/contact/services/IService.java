package com.contact.services;

import java.util.List;
import java.util.Optional;

public interface IService<T> {

	public T save(T t);

	public void delete(Long id);

	public Optional<T> getById(Long id);

	public List<T> getAll();
}
