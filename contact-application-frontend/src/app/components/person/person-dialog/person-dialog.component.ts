import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {PersonService} from '../../../services/person.service';
import {Person} from '../../../model/person.model';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-person-dialog',
  templateUrl: './person-dialog.component.html',
  styleUrls: ['./person-dialog.component.css']
})
export class PersonDialogComponent implements OnInit {

  @Input() person: Person;
  isSaving: boolean;
  message: string;
  sucess: boolean;
  constructor(
    public activeModal: NgbActiveModal,
    private personService: PersonService
  ) { }

  ngOnInit() {
    this.isSaving = false;
    this.sucess = true;
  }

  save() {
    if (this.person.id) {

    } else {
      this.personService.save(this.person).subscribe(response => {
        this.isSaving = true;
        this.activeModal.dismiss();
        console.log(response);
      }, (error: HttpErrorResponse) => {
        this.sucess = false;
        this.message = error.message;
      });
    }
  }

}
