import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonRoutingModule } from './person-routing.module';
import { PersonListComponent } from './person-list/person-list.component';
import { PersonDialogComponent } from './person-dialog/person-dialog.component';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    PersonRoutingModule,
    FormsModule,
    NgbModule
  ],
  declarations: [PersonListComponent, PersonDialogComponent],
  entryComponents: [PersonDialogComponent]
})
export class PersonModule { }
