import { Component, OnInit } from '@angular/core';
import {Person} from '../../../model/person.model';
import {PersonService} from '../../../services/person.service';
import {HttpErrorResponse} from '@angular/common/http';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PersonDialogComponent} from '../person-dialog/person-dialog.component';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.css']
})
export class PersonListComponent implements OnInit {

  listOfPerson: Person[];
  message: string;
  constructor(
    private personService: PersonService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.loadContacts();
  }

  loadContacts() {
    this.personService.findAll().subscribe(response => {
      if (response.status === 200) {
        this.listOfPerson = response.body;
      }
    }, (error: HttpErrorResponse) => {
        this.message = error.message;
    });
  }

  openDialogue(person: Person) {
    const modalref = this.modalService.open(PersonDialogComponent, {});
    if (!person) {
      modalref.componentInstance.person = new Person();
    } else {
      modalref.componentInstance.person = person;
    }
  }

}
