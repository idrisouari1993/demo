export  class Person {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public adresse?: string,
    public email?: string,
    public phone?: string
  ) {}
}
