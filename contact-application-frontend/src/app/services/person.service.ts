import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Person} from '../model/person.model';
import {BASE_URL} from '../utils/app.const';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  private api_url = BASE_URL + '/person';

  constructor(private http: HttpClient) { }

  public save(person: Person): Observable<HttpResponse<any>> {
    return this.http.post(this.api_url, person, {observe: 'response'}).pipe(catchError(this.handleError));
  }
  public update(person: Person): Observable<HttpResponse<any>> {
    return this.http.post(this.api_url, person, {observe: 'response'}).pipe(catchError(this.handleError));
  }
  public findById(id: number): Observable<HttpResponse<Person>> {
    return this.http.get(this.api_url + '/' + id, {observe: 'response'}).pipe(catchError(this.handleError));
  }
  public findAll(): Observable<HttpResponse<Person[]>> {
    return this.http.get<Person[]>(this.api_url, {observe: 'response'}).pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(
      'Something bad happened; please try again later.');
  }
}
